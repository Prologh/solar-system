#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include <iostream>
#include <cmath>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

const int PLANET_MOONS_MAX_AMOUNT = 11;

GLuint programColor, programTexture, shipTexture, moonTexture;
GLuint* planetTextureArray;
glm::mat4* planetMatrixArray;
glm::mat4 shipModelMatrix;
obj::Model shipModel, sphereModel;
Core::Shader_Loader shaderLoader;

enum PlanetsEnum {
	Sun = 0,
	Mercury,
	Venus,
	Earth,
	Mars,
	Jupiter,
	Saturn,
	Uranus,
	Neptune,
	PlanetsAmount,
};

float	angleSpeed,
		moveSpeed,
		moveSpeedBoost,
		moveSpeedBoostOn,
		moveSpeedBoostOff,
		cameraAngle,
		spaceshipCameraZoom,
		spaceshipCameraZoomStep;
bool* keyStates;
bool keyboardLock;

glm::vec3 cameraPos, cameraDir;
glm::mat4 cameraMatrix, perspectiveMatrix;
glm::vec3 lightDir;

void intro() {
	if (keyboardLock && spaceshipCameraZoom < 1.0f)
		spaceshipCameraZoom += spaceshipCameraZoomStep;
	else
		keyboardLock = false;
}

void resetSpaceshipPosition()
{
	keyboardLock = false;
	cameraPos = glm::vec3(-15, 0, 0);
	angleSpeed = 0.05f;
	moveSpeed = 0.1f;
	moveSpeedBoostOn = 3.0f;
	moveSpeedBoostOff = 1.0f;
	cameraAngle = 0;
	spaceshipCameraZoom = 1.0f;
	spaceshipCameraZoomStep = 0.01f;
}

void setStartingOptions() {
	spaceshipCameraZoom = 0.0f;
	keyboardLock = true;
}

//void tryMove(float moveDistance, glm::vec3 direction) {
//	cameraPos += direction * moveDistance;
//	for (int i = 0; i < PlanetsAmount; i++) {
//		float radius = planetMatrixArray[i][0][0];
//		if (shipModelMatrix[0][0] -
//			planetMatrixArray[i][0][0]) {
//			cameraPos -= direction * moveDistance;
//			return;
//		}
//	}
//}

void keyPressed(unsigned char key, int x, int y) {
	keyStates[key] = true;
}

void keyUp(unsigned char key, int x, int y) {
	keyStates[key] = false;
}

void keyOperations()
{
	if (keyStates[27]) { resetSpaceshipPosition(); return; }
	if (!keyboardLock) {
		if (keyStates[32])	moveSpeedBoost = moveSpeedBoostOn;
		else				moveSpeedBoost = moveSpeedBoostOff;
		if (keyStates['a']) cameraAngle -= angleSpeed;
		if (keyStates['d']) cameraAngle += angleSpeed;
		if (keyStates['w'])	cameraPos += cameraDir * moveSpeed * moveSpeedBoost;
			//tryMove(moveSpeed * moveSpeedBoost, cameraDir);
		if (keyStates['s']) cameraPos -= cameraDir * moveSpeed * moveSpeedBoost;
			//tryMove(-moveSpeed * moveSpeedBoost, cameraDir);
		if (keyStates['z']) cameraPos -= glm::cross(cameraDir, glm::vec3(0, 1, 0)) * moveSpeed;
		if (keyStates['x']) cameraPos += glm::cross(cameraDir, glm::vec3(0,1,0)) * moveSpeed;
		if (keyStates['r'] && spaceshipCameraZoom < 1.4f)
			spaceshipCameraZoom += spaceshipCameraZoomStep;
		if (keyStates['f'] && spaceshipCameraZoom - spaceshipCameraZoomStep > 0.75f)
			spaceshipCameraZoom -= spaceshipCameraZoomStep;
	}
}

glm::mat4 createCameraMatrix()
{
	cameraDir = glm::vec3(cosf(cameraAngle), 0.0f, sinf(cameraAngle));
	glm::vec3 up = glm::vec3(0, spaceshipCameraZoom, 0);
	return Core::createViewMatrix(cameraPos, cameraDir, up);
}

void drawObjectColor(obj::Model * model, glm::mat4 modelMatrix, glm::vec3 color)
{
	GLuint program = programColor;

	glUseProgram(program);

	glUniform3f(glGetUniformLocation(program, "objectColor"), color.x, color.y, color.z);
	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectTexture(obj::Model * model, glm::mat4 modelMatrix, GLuint texture)
{
	GLuint program = programTexture;

	glUseProgram(program);

	//glUniform3f(glGetUniformLocation(program, "objectColor"), 0.5f, 0.3f, 0.4f);
	glUniform3f(glGetUniformLocation(
		program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(
		program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(
		program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::SetActiveTexture(texture, "texSampler2D", program, 0);

	Core::DrawModel(model);

	glUseProgram(0);
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	//set the viewport to the current window specifications

	glMatrixMode(GL_PROJECTION);
	//set the matrix to projection

	glLoadIdentity();
	gluPerspective(45, (GLfloat)w / (GLfloat)h, 1.0, 100.0);
	//set the perspective (angle of sight, width, height, , depth)

	glMatrixMode(GL_MODELVIEW);
	//set the matrix back to model

}

glm::mat4 getPlanetMatrix(float scaleFactor, float rotationSpeed,
	float movementSpeed, float distance) {
	float time = glutGet(GLUT_ELAPSED_TIME) / 10000.0f;

	// Translation
	glm::mat4 translation;
	if (movementSpeed != 0.0f && distance > 0.0f) {
		translation[3][0] = sin(time * movementSpeed * 0.1f) * distance;
		translation[3][1] = 0.0f;
		translation[3][2] = cos(time * movementSpeed * 0.1f) * distance;
	}

	// Rotation-Y
	glm::mat4 rotation;
	if (rotationSpeed > 0.0f) {
		rotation[0][0] = cos(time * rotationSpeed * 0.1f);
		rotation[2][0] = sin(time * rotationSpeed * 0.1f);
		rotation[0][2] = -sin(time * rotationSpeed * 0.1f);
		rotation[2][2] = cos(time * rotationSpeed * 0.1f);
	}

	// Scale
	glm::mat4 scale;
	if (scaleFactor != 1.0f) {
		scale[0][0] *= scaleFactor;
		scale[1][1] *= scaleFactor;
		scale[2][2] *= scaleFactor;
	}

	return translation * rotation * scale;
};

glm::mat4 getMoonMatrix(float scaleFactor, float rotationSpeed,
	float movementSpeed, float distance, glm::mat4 sourcePlanetMatrix) {
	float time = glutGet(GLUT_ELAPSED_TIME) / 10000.0f;

	// Translation
	glm::mat4 translation;
	if (movementSpeed != 0.0f && distance > 0.0f) {
		translation[3][0] += cos(time * movementSpeed) * distance;
		translation[3][1] += 0.0f;
		translation[3][2] += sin(time * movementSpeed) * distance;
	}

	// Rotation-Y
	glm::mat4 rotation;
	if (rotationSpeed > 0.0f) {
		rotation[0][0] = cos(time * rotationSpeed);
		rotation[2][0] = sin(time * rotationSpeed);
		rotation[0][2] = -sin(time * rotationSpeed);
		rotation[2][2] = cos(time * rotationSpeed);
	}

	// Scale
	glm::mat4 scale;
	if (scaleFactor != 1.0f) {
		scale[0][0] *= scaleFactor;
		scale[1][1] *= scaleFactor;
		scale[2][2] *= scaleFactor;
	}

	return sourcePlanetMatrix * translation * rotation * scale;
};

	// Generuje wektor pozycji �wiat�a na podstawie podanej macierzy 4 x 4.
glm::vec3 getLightSource(glm::mat4 sourceMatrix) {
	float time = glutGet(GLUT_ELAPSED_TIME) / 10000.0f;

	//return glm::normalize(glm::vec3(1.0f, -0.9f, -1.0f));
	//return glm::normalize(glm::vec3(
	//	sourceMatrix[2][2],
	//	sourceMatrix[1][1],
	//	sourceMatrix[0][0]
	//));
	return glm::normalize(glm::vec3(
			sin(time * 8),
			0,
			cos(time * 8)
	));
}

void renderScene()
{
	keyOperations();

	cameraMatrix = createCameraMatrix();
	perspectiveMatrix = Core::createPerspectiveMatrix();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.29f, 1.0f);

	/*
	planetMatrixArray[PlanetEnum]=	getPlanetMatrix(Scale,	Rotate,	Move,	Distance);*/
	planetMatrixArray[Sun]		=	getPlanetMatrix(10.0f,	1.4f,	0.0f,	0.0f);
	planetMatrixArray[Mercury]	=	getPlanetMatrix(0.68f,	64.0f,	9.0f,	13.0f);
	planetMatrixArray[Venus]	=	getPlanetMatrix(1.15f,	64.0f,	8.5f,	17.5f);
	planetMatrixArray[Earth]	=	getPlanetMatrix(1.1f,	64.0f,	7.9f,	24.0f);
	planetMatrixArray[Mars]		=	getPlanetMatrix(0.98f,	64.0f,	7.2f,	36.0f);
	planetMatrixArray[Jupiter]	=	getPlanetMatrix(6.2f,	64.0f,	5.3f,	56.0f);
	planetMatrixArray[Saturn]	=	getPlanetMatrix(6.9f,	64.0f,	4.9f,	72.0f);
	planetMatrixArray[Uranus]	=	getPlanetMatrix(4.0f,	64.0f,	4.4f,	87.0f);
	planetMatrixArray[Neptune]	=	getPlanetMatrix(0.88f,	64.0f,	3.2f,	104.0f);

	// Narysuj planety
	for (int i = 0; i < PlanetsAmount; i++)
		drawObjectTexture(&sphereModel, planetMatrixArray[i], planetTextureArray[i]);

	// Narysuj ksi�yc ziemi
	drawObjectTexture(
		&sphereModel,
		getMoonMatrix(0.15f, 64.0f, 24.0f, 1.6f, planetMatrixArray[Earth]),
		moonTexture);

	// Narysuj statek
	shipModelMatrix =
		glm::translate(cameraPos + cameraDir * 0.5f + glm::vec3(0, -0.25, 0)) *
		glm::rotate(-cameraAngle + glm::radians(90.0f), glm::vec3(0, 1, 0)) *
		glm::scale(glm::vec3(0.25f));
	drawObjectTexture(&shipModel, shipModelMatrix, shipTexture);

	// Generuje pozycje �r�d�a �wiat�a
	lightDir = cameraDir;

	intro();
	glutSwapBuffers();
}

void init()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);			//enable multisampling
	glEnable(GL_DEPTH_TEST);			//enable the depth testing
	//glEnable(GL_CULL_FACE);			//enable face culling
	glEnable(GL_LIGHTING);				//enable the lighting
	glEnable(GL_LIGHT0);				//enable LIGHT0, Diffuse Light
	glEnable(GL_LIGHT1);				//enable LIGHT1, Ambient Light
	glShadeModel(GL_SMOOTH);			//set the shader to smooth shader

	programColor = shaderLoader.CreateProgram(
		"shaders/shader_color.vert", "shaders/shader_color.frag");
	programTexture = shaderLoader.CreateProgram(
		"shaders/shader_tex.vert", "shaders/shader_tex.frag");

	sphereModel = obj::loadModelFromFile("models/sphere.obj");
	shipModel = obj::loadModelFromFile("models/spaceship.obj");

	planetMatrixArray = new glm::mat4[PlanetsAmount];
	planetTextureArray = new GLuint[PlanetsAmount];
	planetTextureArray[Sun] =		Core::LoadTexture("textures/sun.png");
	planetTextureArray[Mercury] =	Core::LoadTexture("textures/mercury.png");
	planetTextureArray[Venus] =		Core::LoadTexture("textures/venus.png");
	planetTextureArray[Earth] =		Core::LoadTexture("textures/earth2.png");
	planetTextureArray[Mars] =		Core::LoadTexture("textures/mars.png");
	planetTextureArray[Jupiter] =	Core::LoadTexture("textures/jupiter.png");
	planetTextureArray[Saturn] =	Core::LoadTexture("textures/saturn.png");
	planetTextureArray[Uranus] =	Core::LoadTexture("textures/uranus.png");
	planetTextureArray[Neptune] =	Core::LoadTexture("textures/neptune.png");

	moonTexture = Core::LoadTexture("textures/moon.png");
	shipTexture = Core::LoadTexture("textures/spaceship.png");

	keyStates = new bool[255]{ false };
	resetSpaceshipPosition();
	setStartingOptions();
}

void shutdown()
{
	shaderLoader.DeleteProgram(programColor);
	shaderLoader.DeleteProgram(programTexture);
	shaderLoader.DeleteProgram(moonTexture);
	delete[] planetTextureArray;
	delete[] planetMatrixArray;
	delete[] keyStates;
}

void idle()
{
	glutPostRedisplay();
}

int main(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(500, 150);
	glutInitWindowSize(700, 700);
	glutCreateWindow("Solar System OpenGL");
	glewInit();

	init();
	glutDisplayFunc(renderScene);
	glutIdleFunc(idle);

	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyUp);

	glutMainLoop();

	shutdown();
	return 0;
}
