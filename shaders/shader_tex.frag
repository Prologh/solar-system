#version 430 core

uniform vec3 objectColor;
uniform vec3 lightDir;
uniform sampler2D texSampler2D;

in vec3 interpNormal;
in vec2 texCoord;

void main()
{
	vec3 normal = normalize(interpNormal);
	float diffuse = max(dot(normal, -lightDir), 0.0);
	vec4 textureColor = texture2D(texSampler2D, texCoord);
	gl_FragColor = vec4(textureColor.xyz * diffuse, 1.0);
}
